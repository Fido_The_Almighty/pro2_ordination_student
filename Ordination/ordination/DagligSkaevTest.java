package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {
	private Patient p;
	private Laegemiddel lm;

	@Before
	public void setUp() throws Exception {
		p = new Patient("123456-9898", "Lars H", 79.7);
		lm = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
	}

	@Test
	public void testSamletDosis() {

		// TestCase 1
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), lm, p);

		assertEquals(0, ds.samletDosis(), 0.001);

		// TestCase 2
		ds.opretDosis(LocalTime.of(8, 0), 2);
		ds.opretDosis(LocalTime.of(12, 0), 3);

		assertEquals(20, ds.samletDosis(), 0.001);

		// TestCase 3
		ds = new DagligSkaev(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 20), lm, p);
		ds.opretDosis(LocalTime.of(8, 0), 2);
		ds.opretDosis(LocalTime.of(12, 0), 3);
		ds.opretDosis(LocalTime.of(14, 0), 1);
		ds.opretDosis(LocalTime.of(16, 0), 2);

		assertEquals(56, ds.samletDosis(), 0.001);

		// TestCase 4
		ds.opretDosis(LocalTime.of(22, 0), 2.5);
		assertEquals(73.5, ds.samletDosis(), 0.001);

	}

	@Test
	public void testDoegnDosis() {

		// TestCase 1
		DagligSkaev ds = new DagligSkaev(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), lm, p);

		assertEquals(0, ds.doegnDosis(), 0.001);

		// TestCase 2
		ds.opretDosis(LocalTime.of(8, 0), 2);
		ds.opretDosis(LocalTime.of(12, 0), 3);

		assertEquals(5, ds.doegnDosis(), 0.001);

		// TestCase 3
		ds.opretDosis(LocalTime.of(14, 0), 1);
		ds.opretDosis(LocalTime.of(16, 0), 2);

		assertEquals(8, ds.doegnDosis(), 0.001);

		// TestCase 3
		ds.opretDosis(LocalTime.of(22, 0), 2.5);
		assertEquals(10.5, ds.doegnDosis(), 0.001);

	}

	@Test
	public void testOpretDosis() {

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), lm, p);

		// TestCase 1
		Dosis d;
		try {

			d = ds.opretDosis(LocalTime.of(8, 30), -1);

		} catch (RuntimeException e) {

			assertEquals("Antallet af enheder skal være positivt", e.getMessage());
		}

		// TestCase 2
		d = ds.opretDosis(LocalTime.of(8, 30), 0);

		assertNotNull(d);

		// TestCase 3
		d = ds.opretDosis(LocalTime.of(11, 45), 2);

		assertNotNull(d);

		// TestCase 4
		try {
			d = ds.opretDosis(LocalTime.of(45, 75), 2);

			fail("");
		} catch (RuntimeException e) {

			assertEquals("java.time.DateTimeException", e.getClass().getName());

		}

	}

}
