package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class PNTest {

	private Patient p;
	private Laegemiddel l;
	private PN pn;

	@Before
	public void setUp() throws Exception {
		p = new Patient("123456-9898", "Lars H", 79.7);
		l = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		pn = new PN(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 21), l, 2, p);
	}

	@Test
	public void testSamletDosis() {
		PN pn1 = new PN(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), l, 0, p);
		assertEquals(0, pn1.samletDosis(), 0.001);

		pn.givDosis(LocalDate.of(2000, 12, 17));
		assertEquals(2, pn.samletDosis(), 0.001);

		PN pn2 = new PN(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), l, 4, p);
		pn2.givDosis(LocalDate.of(2000, 12, 17));
		pn2.givDosis(LocalDate.of(2000, 12, 17));
		pn2.givDosis(LocalDate.of(2000, 12, 17));
		assertEquals(12, pn2.samletDosis(), 0.001);

		PN pn3 = new PN(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 21), l, 5, p);
		pn3.givDosis(LocalDate.of(2000, 12, 20));
		pn3.givDosis(LocalDate.of(2000, 12, 20));
		assertEquals(10, pn3.samletDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis() {
		assertEquals(0, pn.doegnDosis(), 0.001); // TC1

		PN pn2 = new PN(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), l, 3, p);
		pn2.givDosis(LocalDate.of(2000, 12, 17));
		pn2.givDosis(LocalDate.of(2000, 12, 17));
		pn2.givDosis(LocalDate.of(2000, 12, 17));
		assertEquals(2.25, pn2.doegnDosis(), 0.001);

		PN pn3 = new PN(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 21), l, 1, p);
		pn3.givDosis(LocalDate.of(2000, 12, 20));
		pn3.givDosis(LocalDate.of(2000, 12, 20));
		pn3.givDosis(LocalDate.of(2000, 12, 20));
		assertEquals(0.375, pn3.doegnDosis(), 0.001);

		PN pn4 = new PN(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 15), l, 0, p);
		pn4.givDosis(LocalDate.of(2000, 12, 15));
		pn4.givDosis(LocalDate.of(2000, 12, 15));
		pn4.givDosis(LocalDate.of(2000, 12, 15));
		assertEquals(0, pn4.doegnDosis(), 0.001);
	}

	@Test
	public void testGivDosis() {
		assertEquals(true, pn.givDosis(LocalDate.of(2000, 12, 14))); // TC1
		assertEquals(true, pn.givDosis(LocalDate.of(2000, 12, 21))); // TC2
		assertEquals(true, pn.givDosis(LocalDate.of(2000, 12, 17))); // TC3
		assertEquals(false, pn.givDosis(LocalDate.of(2000, 11, 14))); // TC4

	}
}
