package ordination;

import java.time.LocalDate;

public class DagligFast extends Ordination {

	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel præparat, Patient patient, Dosis[] doser) {
		super(startDen, slutDen, præparat);
		patient.addOrdination(this);
		this.doser = doser;
	}

	public Dosis[] getDoser() {

		return doser;
	}

	@Override

	public double samletDosis() {
		return doegnDosis() * antalDage();
	}

	@Override
	public double doegnDosis() {
		double dosis = 0;

		for (Dosis d : doser) {

			dosis += d.getAntal();

		}

		return dosis;

	}

	@Override
	public String getType() {
		return null;
	}
}