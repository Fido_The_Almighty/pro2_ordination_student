package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {

	private double antalEnheder;
	private ArrayList<LocalDate> datoerGivet = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel præparat, double antalEnheder, Patient patient) {

		super(startDen, slutDen, præparat);
		this.antalEnheder = antalEnheder;
		patient.addOrdination(this);

	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		// TODO
		boolean givet = true;

		if (givesDen.isBefore(this.getStartDen()) || givesDen.isAfter(this.getSlutDen())) {

			givet = false;

		} else {

			datoerGivet.add(givesDen);

		}

		return givet;
	}

	public double doegnDosis() {
		// TODO
		return samletDosis() / ((double) antalDage());
	}

	public double samletDosis() {
		// TODO

		return ((double) datoerGivet.size()) * antalEnheder;

	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet() {
		// TODO
		return datoerGivet.size();
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	public String getType() {

		return this.getClass().getName();

	}

}
