package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	// TODO

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel præparat, Patient patient) {

		super(startDen, slutDen, præparat);
		patient.addOrdination(this);

	}

	@Override
	public double samletDosis() {

		return doegnDosis() * antalDage();

	}

	@Override
	public double doegnDosis() {

		double dosis = 0;

		for (Dosis d : doser) {

			dosis += d.getAntal();

		}

		return dosis;

	}

	@Override
	public String getType() {

		return DagligSkaev.class.getName();

	}

	// --------------------------------------
	// Komposition --> 0..* Dosis

	private ArrayList<Dosis> doser = new ArrayList<>();

	public ArrayList<Dosis> getDoser() {

		return new ArrayList<>(doser);

	}

	/**
	 * 
	 * @param tid   reelt tidspunkt*
	 * 
	 * @param antal >= 0
	 * @return
	 */
	public Dosis opretDosis(LocalTime tid, double antal) throws RuntimeException {
		// TODO

		Dosis d = new Dosis(tid, antal);

		if (antal >= 0) {

			int i = 0;
			boolean found = false;

//            //
//             * Tjek om der allerede eksisterer en dosis på det angivne tidspunkt.
//             * 
//             *//
			while (!found && i < doser.size()) {

				if (d.getTid().compareTo(doser.get(i).getTid()) == 0) {

					found = true;

				}

				i++;
			}

			if (!found) {

				doser.add(d);

			}
		} else {
			throw new IllegalArgumentException("Antallet af enheder skal være positivt");
		}
		return d;
	}

}
