package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

public class DagligFastTest {

	private Dosis d1, d2, d3, d4;
	private Dosis[] doser;
	private Patient p;
	private Laegemiddel lm;

	@Before
	public void setUp() throws Exception {

		doser = new Dosis[4];

		d1 = new Dosis(LocalTime.of(8, 0), 0);
		d2 = new Dosis(LocalTime.of(12, 0), 0);
		d3 = new Dosis(LocalTime.of(18, 0), 0);
		d4 = new Dosis(LocalTime.of(22, 0), 0);

		doser[0] = d1;
		doser[1] = d2;
		doser[2] = d3;
		doser[3] = d4;

		p = new Patient("123456-9898", "Lars H", 79.7);
		lm = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");

	}

	@Test
	public void testSamletDosis() {

		// TestCase 1
		DagligFast df = new DagligFast(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), lm, p, doser);

		assertEquals(0, df.samletDosis(), 0.001);

		// TestCase 1
		d1.setAntal(2);
		d2.setAntal(0);
		d3.setAntal(0);
		d4.setAntal(3);

		assertEquals(20, df.samletDosis(), 0.001);

		// TestCase 3
		df = new DagligFast(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 20), lm, p, doser);

		d1.setAntal(1.5);
		d2.setAntal(1);
		d3.setAntal(1);
		d4.setAntal(2);

		assertEquals(38.5, df.samletDosis(), 0.001);

	}

	@Test
	public void testDoegnDosis() {

		// TestCase 1

		DagligFast df = new DagligFast(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 15), lm, p, doser);

		assertEquals(0, df.doegnDosis(), 0.001);

		// TestCase 2
		d1.setAntal(2);
		d2.setAntal(0);
		d3.setAntal(0);
		d4.setAntal(3);

		assertEquals(5, df.doegnDosis(), 0.001);

		// TestCase 3
		d1.setAntal(1.5);
		d2.setAntal(1);
		d3.setAntal(1);
		d4.setAntal(2);

		assertEquals(5.5, df.doegnDosis(), 0.001);

	}
}
