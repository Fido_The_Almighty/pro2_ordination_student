package controller;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ControllerTest {
	Controller controller;

	@Before
	public void setUp() throws Exception {
		controller = Controller.getTestController();

	}

	@Test
	public void testOpretPNOrdination() {
		// Testcase 1
		Patient p = controller.opretPatient("123456-9898", "Lars H", 79.7);
		Laegemiddel lm = controller.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = controller.opretPNOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p, lm, 0);

		assertNotNull(pn);
		assertEquals(0, pn.getAntalEnheder(), 0.001);

		// Testcase 2
		try {
			controller.opretPNOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), null, lm, 0);
			fail("");
		} catch (RuntimeException e) {
			assertEquals("Patient mangler", e.getMessage());
		}

		// Testcase 3
		try {
			controller.opretPNOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p, null, 0);
			fail("");
		} catch (RuntimeException e) {
			assertEquals("Laegemiddel mangler", e.getMessage());
		}

		// Testcase 4
		try {
			controller.opretPNOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p, lm, -1);
			fail("");
		} catch (RuntimeException e) {
			assertEquals("Antal skal være positiv", e.getMessage());
		}

		// Testcase 5
		try {
			controller.opretPNOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 13), p, lm, 0);
			fail("");
		} catch (RuntimeException e) {
			assertEquals("Slutdato er før startdato", e.getMessage());
		}

		// Testcase 6
		PN pn2 = controller.opretPNOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p, lm, 3);
		assertNotNull(pn2);
		assertEquals(3, pn2.getAntalEnheder(), 0.001);
	}

	@Test
	public void testOpretDagligFastOrdination() {
		// Testcase 1
		Patient p = controller.opretPatient("123456-9898", "Lars H", 79.7);
		Laegemiddel lm = controller.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		DagligFast df = controller.opretDagligFastOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p,
				lm, 1, 1, 1, 1);

		assertEquals(4, df.getDoser().length, 0.001);

		// Testcase 2
		try {
			controller.opretDagligFastOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), null, lm, 0, 0,
					0, 0);
			fail("");
		} catch (RuntimeException e) {
			assertEquals("Patient mangler", e.getMessage());
		}

		// Testcase 3
		try {
			controller.opretDagligFastOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p, null, 0, 0,
					0, 0);
			fail("");
		} catch (RuntimeException e) {
			assertEquals("Laegemiddel mangler", e.getMessage());
		}

		// Testcase 4
		try {
			controller.opretDagligFastOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p, lm, -1, 0,
					0, 0);
			fail("");
		} catch (RuntimeException e) {
			assertEquals("Antal skal være positiv", e.getMessage());
		}

		// Testcase 5
		try {
			controller.opretDagligFastOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 13), p, lm, 0, 0, 0,
					0);
			fail("");
		} catch (RuntimeException e) {
			assertEquals("Slutdato er før startdato", e.getMessage());
		}

		// Testcase 6
		try {
			controller.opretDagligFastOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p, lm, 1, 1, 1,
					2);
			fail("");
		} catch (RuntimeException e) {
			assertEquals("Mængden af doser overgår maksimalen for dagen", e.getMessage());
		}
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		// Testcase 1
		LocalTime[] klokkeSlet = { LocalTime.of(8, 0), LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(20, 0) };
		double[] antal = { 1, 2, 1, 1 };
		Patient p = controller.opretPatient("123456-9898", "Lars H", 79.7);
		Laegemiddel lm = controller.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		DagligSkaev ds = controller.opretDagligSkaevOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17),
				p, lm, klokkeSlet, antal);
		assertNotNull(ds);

		// Testcase 2
		try {
			klokkeSlet[1] = LocalTime.of(24, 10);
			fail("");
		} catch (RuntimeException e) {
			assertEquals("java.time.DateTimeException", e.getClass().getName());
		}

		// Testcase 3

		klokkeSlet[1] = LocalTime.of(8, 10);
		antal[1] = -2;
		try {
			controller.opretDagligSkaevOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p, lm,
					klokkeSlet, antal);
		} catch (RuntimeException e) {
			assertEquals("Antal af enheder skal være positivt", e.getMessage());
		}

		// Testcase 4
		antal[1] = 2;
		double[] antal2 = Arrays.copyOf(antal, 5);
		try {
			controller.opretDagligSkaevOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p, lm,
					klokkeSlet, antal2);
		} catch (RuntimeException e) {
			assertEquals("Der er ikke samme antal klokkeslet som enheder", e.getMessage());
		}

		// Testcase 5
		try {
			controller.opretDagligSkaevOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), null, lm,
					klokkeSlet, antal);
		} catch (RuntimeException e) {
			assertEquals("Patient mangler", e.getMessage());
		}

		// Testcase 6
		try {
			controller.opretDagligSkaevOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p, null,
					klokkeSlet, antal);
		} catch (RuntimeException e) {
			assertEquals("Laegemiddel mangler", e.getMessage());
		}

		// Testcase 7
		try {
			controller.opretDagligSkaevOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 13), p, null,
					klokkeSlet, antal);
		} catch (RuntimeException e) {
			assertEquals("Slutdato er før startdato", e.getMessage());
		}
	}

	@Test
	public void testOrdinationPNAnvendt() {
		Patient p = new Patient("123456-9898", "Lars H", 79.7);
		Laegemiddel lm = new Laegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		PN pn = new PN(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 20), lm, 0, p);
		controller = Controller.getTestController();
		assertEquals(0, pn.getAntalGangeGivet(), 0.001);

		PN pn1 = new PN(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 20), lm, 2, p);
		pn1.givDosis(LocalDate.of(2000, 12, 17));
		assertEquals(1, pn1.getAntalGangeGivet(), 0.001);

		PN pn2 = new PN(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 20), lm, 4, p);
		pn2.givDosis(LocalDate.of(2000, 12, 20));
		pn2.givDosis(LocalDate.of(2000, 12, 20));
		assertEquals(2, pn2.getAntalGangeGivet(), 0.001);

		PN pn3 = new PN(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 20), lm, 6, p);
		pn3.givDosis(LocalDate.of(2000, 12, 20));
		pn3.givDosis(LocalDate.of(2000, 12, 21));
		pn3.givDosis(LocalDate.of(2000, 12, 21));

		try {
			controller.ordinationPNAnvendt(pn3, LocalDate.of(2000, 12, 21));
			fail("");
		} catch (IllegalArgumentException e) {
			assertEquals("Slutdato er før startdato", e.getMessage());
		}
	}

	@Test
	public void testAnbefaletDosisPrDoegn() {
		// Testcase 1
		Patient p = controller.opretPatient("123456-9898", "Lars H", 25);
		Laegemiddel lm = controller.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		double enheder = controller.anbefaletDosisPrDoegn(p, lm);

		assertEquals(0.625, enheder, 0.001);

		// Testcase 2
		Patient p2 = controller.opretPatient("123456-9898", "Lars H", 75);
		double enheder2 = controller.anbefaletDosisPrDoegn(p2, lm);

		assertEquals(1.875, enheder2, 0.001);

		// Testcase 3
		Patient p3 = controller.opretPatient("123456-9898", "Lars H", 120);
		double enheder3 = controller.anbefaletDosisPrDoegn(p3, lm);

		assertEquals(3, enheder3, 0.001);

		// Testcase 4
		Patient p4 = controller.opretPatient("123456-9898", "Lars H", 150);
		double enheder4 = controller.anbefaletDosisPrDoegn(p4, lm);

		assertEquals(3.75, enheder4, 0.001);
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel() {
		// Testcase 1
		Patient p1 = controller.opretPatient("121256-0512", "Jane Jensen", 20);
		Patient p2 = controller.opretPatient("121256-0512", "Jane Jensen", 30);
		Patient p3 = controller.opretPatient("121256-0512", "Jane Jensen", 33);
		Patient p4 = controller.opretPatient("070985-1153", "Finn Madsen", 70);
		Patient p5 = controller.opretPatient("050972-1233", "Hans Jørgensen", 55);
		Patient p6 = controller.opretPatient("090149-2529", "Ib Hansen", 120);
		Laegemiddel fuci = controller.opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		Laegemiddel ace = controller.opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		Laegemiddel para = controller.opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		Laegemiddel meth = controller.opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");
		controller.opretDagligFastOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p1, fuci, 1, 1, 1,
				1);
		controller.opretDagligFastOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p2, fuci, 1, 1, 1,
				1);
		controller.opretDagligFastOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p3, fuci, 1, 1, 1,
				1);
		int tc1 = controller.antalOrdinationerPrVægtPrLægemiddel(0, 45, fuci);
		assertEquals(3, tc1);

		// Testcase 2
		int tc2 = controller.antalOrdinationerPrVægtPrLægemiddel(0, 60, para);
		assertEquals(0, tc2);

		// Testcase 3
		controller.opretPNOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p4, para, 2);
		controller.opretPNOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p5, para, 2);
		int tc3 = controller.antalOrdinationerPrVægtPrLægemiddel(55, 90, para);
		assertEquals(2, tc3);

		// Testcase 4
		try {
			controller.antalOrdinationerPrVægtPrLægemiddel(-20, 45, fuci);
		} catch (RuntimeException e) {
			assertEquals("Startvægten skal være positiv", e.getMessage());
		}

		// Testcase 5
		try {
			controller.antalOrdinationerPrVægtPrLægemiddel(0, 45, null);
		} catch (RuntimeException e) {
			assertEquals("Laegemiddel mangler", e.getMessage());
		}

		// Testcase 6
		try {
			controller.antalOrdinationerPrVægtPrLægemiddel(90, 45, para);
		} catch (RuntimeException e) {
			assertEquals("Startvægt må ikke overstige slutvægt", e.getMessage());
		}

		// Testcase 7
		LocalTime[] klokkeSlet = { LocalTime.of(8, 0), LocalTime.of(10, 0), LocalTime.of(14, 0), LocalTime.of(20, 0) };
		double[] antal = { 1, 2, 1, 1 };
		controller.opretDagligSkaevOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p1, ace,
				klokkeSlet, antal);
		int tc4 = controller.antalOrdinationerPrVægtPrLægemiddel(10, 25, fuci);
		assertEquals(1, tc4);

		// Testcase 8
		controller.opretDagligSkaevOrdination(LocalDate.of(2000, 12, 14), LocalDate.of(2000, 12, 17), p6, meth,
				klokkeSlet, antal);
		int tc5 = controller.antalOrdinationerPrVægtPrLægemiddel(115, 135, meth);
		assertEquals(1, tc5);

	}

}
